define([${modules}], function(${moduleParameters}) {
   
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType Portlet
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
   
    /**
     * Entry point for the Portlet.
     * 
     * @governance XXX
	 *
     * @param params {Object}
     * @param params.portlet {Portlet} The portlet object used for rendering
     * @param params.column {Number} Specifies whether portlet is placed in left (1), center (2) or right (3) column of the dashboard
     * @param [params.entity] {String} (For customer portlets only) references the customer ID for the selected customer
     * 
	 * @return {void}
	 *
     * @since 2015.2
	 *
	 * @static
	 * @function render
     */
    function render(params) {

    }

	exports.render = render;
	return exports;
});
