var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
