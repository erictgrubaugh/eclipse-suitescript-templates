var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * <code>GET</code> request handler
	 *
	 * @governance XXX
	 *
	 * @param dataIn {Object} Parameter object
	 *
	 * @return {Object} Output object 
	 *
	 * @static
	 * @function get
	 */
	function get(dataIn) {
		
		return {};
	}

	/**
	 * <code>POST</code> request handler
	 *
	 * @governance XXX
	 *
	 * @param dataIn {Object} Parameter object
	 *
	 * @return {Object} Output object 
	 *
	 * @static
	 * @function post
	 */
	function post(dataIn) {
		
		return {};
	}

	/**
	 * <code>DELETE</code> request handler
	 *
	 * @governance XXX
	 *
	 * @param dataIn {Object} Parameter object
	 *
	 * @return {void}
	 *
	 * @static
	 * @function del
	 */
	function del(dataIn) {

	}

	/**
	 * <code>PUT</code> request handler
	 *
	 * @governance XXX
	 *
	 * @param dataIn {Object} Parameter object
	 *
	 * @return {Object} Output object 
	 *
	 * @static
	 * @function put
	 */
	function put(dataIn) {
		
		return {};
	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
