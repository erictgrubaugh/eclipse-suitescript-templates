
	/**
	 * Event fired just before a database write operation
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 * 
	 * @param {String} type Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>delete</li>
	 * <li>xedit</li>
	 * <li>approve</li>
	 * <li>cancel</li>
	 * <li>reject (SO, ER, Time Bill, PO & RMA only)</li>
	 * <li>pack</li>
	 * <li>ship (IF only)</li>
	 * <li>markcomplete (Call, Task)</li>
	 * <li>reassign (Case)</li>
	 * <li>editforecast (Opp, Estimate)</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeSubmit
	 */
	function beforeSubmit(type){
	 
	}
