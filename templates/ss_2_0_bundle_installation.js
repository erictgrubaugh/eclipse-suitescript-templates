define([${modules}], function(${moduleParameters}) {
   
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType BundleInstallationScript
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
	
    /**
     * Executes after a bundle is installed for the first time in a target account.
     *
	 * @governance XXX
	 *
     * @param params {Object}
     * @param params.version {Number} Version of the bundle being installed
     *
	 * @return {void}
	 *
     * @since 2016.1
	 *
	 * @static
	 * @function beforeInstall
     */
    function beforeInstall(params) {

    }

    /**
     * Executes after a bundle in a target account is updated.
     *
	 * @governance XXX
     *
     * @param params {Object}
     * @param params.version {Number} Version of the bundle being installed
     *
	 * @return {void}
	 *
     * @since 2016.1
	 *
	 * @static
	 * @function afterInstall
     */
    function afterInstall(params) {

    }

    /**
     * Executes before a bundle is installed for the first time in a target account.
     *
	 * @governance XXX
     *
     * @param params {Object}
     * @param params.fromVersion {Number} Version currently installed
     * @param params.toVersion {Number} New version of the bundle being installed
     *
	 * @return {void}
	 *
     * @since 2016.1
	 *
	 * @static
	 * @function beforeUpdate
     */
    function beforeUpdate(params) {

    }

    /**
     * Executes before a bundle is uninstalled from a target account.
     *
     * @param params {Object}
     * @param params.fromVersion {Number} Version currently installed
     * @param params.toVersion {Number} New version of the bundle being installed
     *
	 * @return {void}
	 * 
     * @since 2016.1
	 *
	 * @static
	 * @function afterUpdate
     */
    function afterUpdate(params) {

    }

    /**
     * Executes before a bundle in a target account is updated.
     *
	 * @governance XXX
	 *
     * @param params {Object}
     * @param params.version {Number} Version of the bundle being unistalled
     *
	 * @return {void}
	 * 
     * @since 2016.1
	 *
	 * @static
	 * @function beforeUninstall
     */
    function beforeUninstall(params) {

    }
    
	exports.beforeInstall = beforeInstall;
	exports.afterInstall = afterInstall;
	exports.beforeUpdate = beforeUpdate;
	exports.afterUpdate = afterUpdate;
	exports.beforeUninstall = beforeUninstall;
    return exports;
});
