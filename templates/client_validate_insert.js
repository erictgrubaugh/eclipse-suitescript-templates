
	/**
	 * Ensures the line item being inserted is acceptable
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {Boolean} <code>true</code> to continue line item insert, <code>false</code> to abort insert
	 *
	 * @static
	 * @function validateInsert
	 */
	function validateInsert(type){
	  
		return true;
	}
