This project defines custom SuiteScript templates for use with the Eclipse-based
SuiteCloud IDE.

### Installation
1. Either `clone` this project, or download and extract the zip to a location of your choosing
    * You will need to keep this directory in existence, so put it in a safe place
1. Launch Eclipse and open the Preferences
1. *Preferences* > *NetSuite* > *SuiteScript* > *Code Templates*
1. Select the `templates` directory located in the extracted directory
1. Click OK and close the Preferences dialog
1. Generate a new template and see the custom template result

### How they work

There are templates for SuiteScript 1.0, for SuiteScript 2.0, and for the
SuiteCloud Development Framework XML Objects.

For each of the SuiteScript languages, there is a `templates.xml` file.

The general format of templates.xml is:

```xml
<configuration>
	<templates>
		<template label="TEXT YOU WANT IN DROPDOWN"
				defaultFilename="DEFAULT NAME FOR FILE"
				typesControl="radio|checkbox"
				headerFilename="PATH/TO/FILE/HEADER"
				startFilename="PATH/TO/START/FILE"
				endFilename="PATH/TO/END/FILE"
				rename="false">
			<types>
				<files label="TEXT LABEL FOR CHECKBOX" bodyFilename="PATH/TO/FILE/WHEN/SELECTED" />
			</types>
		</template>
	</templates>
</configuration>
```

See the `examples` directory in this project for what the final output of each
file might look like.

For scripts that only have one entry point method (e.g. Suitelet, Portlet,
Scheduled), you use `radio` for the `typesControl` setting and just have a
single `<file>` tag under `<types>`. For those that have multiple entry points
to choose from (e.g. Client, Map/Reduce, User Event), you use `checkbox` for the
`typesControl` and then list each option that you want using `<file>` tags under
`<types>`.

I do not actually know what the `rename` setting does.

The basic file structure of the generated file will be:

```
/* CONTENTS OF HEADER FILE */

/* CONTENTS OF START FILE */

/* CONTENTS OF ENTRY POINT 1 FILE */

/* CONTENTS OF ENTRY POINT 2 FILE */

/* ... */

/* CONTENTS OF ENTRY POINT N FILE */

/* CONTENTS OF END FILE */
```

The SuiteScript 2.0 templates only have one body template file per script type,
rather than one file per entry point method. They can still define start and end
templates per script type.

You can customize any of the files in the templates directory, and Eclipse will
reflect the changes the next time you generate a file with that template. You
do not need to restart Eclipse.

### Known Issues

* *Map/Reduce* is not selectable as a script type when creating a file
* *Suitelet 2.0* and *Bundle Installation 2.0* script types use 1.0 instead of 2.0 templates

Neither of these are issues with the templates but rather with the SuiteCloud
plugin itself. I have reported these issues
[in the User Group](https://usergroup.netsuite.com/users/forum/platform-areas/customization/suitecloud-ide/415936-suitescript-2-0-template-errors#post415936).
