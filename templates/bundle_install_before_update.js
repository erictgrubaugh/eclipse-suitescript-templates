
	/**
	 * Executed before a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 *
	 * @param fromVersion {Number} the version of the bundle that is currently installed in the target account
	 * @param toVersion {Number} the version of the bundle that will be installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeUpdate
	 */
	function beforeUpdate(fromVersion, toVersion) {
	 
	}
