
	/**
	 * Event fired when a line item is initially selected
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function lineInit
	 */
	function lineInit(type) {
		 
	}
