var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Portlet entry point
	 *
	 * @governance XXX
	 *
	 * @param portletObj {nlobjPortlet} Current portlet object
	 * @param column {Number} Column position index:
	 * <ul>
	 * <li>1 = left</li>
	 * <li>2 = middle</li>
	 * <li>3 = right</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function render
	 */
	function render(portletObj, column) {

	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
