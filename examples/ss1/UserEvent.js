var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Event fired after a database read operation before control is returned to the request client
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 *
	 * @param type {String} Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>view</li>
	 * <li>copy</li>
	 * <li>print</li>
	 * <li>email</li>
	 * </ul>
	 * @param form {nlobjForm} Current form
	 * @param request {nlobjRequest} Request object
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeLoad
	 */
	function beforeLoad(type, form, request){
	 
	}

	/**
	 * Event fired just before a database write operation
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 * 
	 * @param {String} type Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>delete</li>
	 * <li>xedit</li>
	 * <li>approve</li>
	 * <li>cancel</li>
	 * <li>reject (SO, ER, Time Bill, PO & RMA only)</li>
	 * <li>pack</li>
	 * <li>ship (IF only)</li>
	 * <li>markcomplete (Call, Task)</li>
	 * <li>reassign (Case)</li>
	 * <li>editforecast (Opp, Estimate)</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeSubmit
	 */
	function beforeSubmit(type){
	 
	}

	/**
	 * Event fired after a database write operation before control is returned to the request client
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 * 
	 * @param type {String} Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>delete</li>
	 * <li>xedit</li>
	 * <li>approve</li>
	 * <li>cancel</li>
	 * <li>reject (SO, ER, Time Bill, PO & RMA only)</li>
	 * <li>pack</li>
	 * <li>ship (IF only)</li>
	 * <li>dropship</li>
	 * <li>specialorder</li>
	 * <li>orderitems (PO only)</li>
	 * <li>paybills (vendor payments)</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterSubmit
	 */
	function afterSubmit(type){
	  
	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
