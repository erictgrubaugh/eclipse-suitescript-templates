define([], function() {

    /**
     * _moduleDesc
     * 
     * @exports _moduleId
     * 
     * @NApiVersion 2.x
     * @NScriptType MassUpdateScript
     * @NModuleScope XXX
     * 
     * @requires _lib
     * 
     * @copyright ${year} Stoic Software
     * @author ${author}
     */
    var exports = {};

    /**
     * Definition of Mass Update trigger point.
     * 
     * @governance XXX
     * 
     * @param params
     *        {Object}
     * @param params.type
     *        {String} Record type of the record being processed by the mass
     *        update
     * @param params.id
     *        {Number} ID of the record being processed by the mass update
     * 
     * @return {void}
     * 
     * @since 2016.1
     * 
     * @static
     * @function each
     */
    function each(params) {

    }

    exports.each = each;
    return exports;
});
