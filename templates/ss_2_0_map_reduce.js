define([${modules}], function(${moduleParameters}) {
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType MapReduceScript
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};

	/**
     * @typedef {Object} ObjectRef
     * @property id {Number} Internal ID of the record instance
     * @property type {String} Record type id
     */
	 	 
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
	 *
     * @governance XXX
	 *
     * @return {Array|Object|Search|ObjectRef} inputSummary
	 *
     * @since 2015.1
	 *
	 * @static
	 * @function getInputData
     */
    function getInputData() {

    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @governance XXX
	 *
     * @param context {MapSummary} Data collection containing the key/value pairs to process through the map stage
	 *
	 * @return {void}
	 *
     * @since 2015.1
	 *
	 * @static
	 * @function map
     */
    function map(context) {

    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @governance XXX
	 *
     * @param context {ReduceSummary} Data collection containing the groups to process through the reduce stage
     * 
	 * @return {void}
	 *
     * @since 2015.1
	 *
	 * @static
	 * @function reduce
     */
    function reduce(context) {

    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @governance XXX
	 *
     * @param summary {Summary} Holds statistics regarding the execution of a map/reduce script
     * 
	 * @return {void}
	 *
     * @since 2015.1
	 *
	 * @static
	 * @function summarize
     */
    function summarize(summary) {

    }

	exports.getInputData = getInputData;
    exports.map = map;
    exports.reduce = reduce;
    exports.summarize = summarize;
	return exports;
});
