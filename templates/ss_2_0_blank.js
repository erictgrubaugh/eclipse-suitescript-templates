define([${modules}], function(${moduleParameters}) {
   
   /**
    * _moduleDesc
	*
	* @exports _moduleId
	*
    * @NApiVersion 2.x
	* @NModuleScope XXX
	*
	* @requires _lib
	*
	* @copyright ${year} Stoic Software
	* @author ${author}
    */
   var exports = {};
   
   // XXX Add module code
   
   // XXX Ensure proper exports
   
   return exports; 
});
