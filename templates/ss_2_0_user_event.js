define([${modules}], function(${moduleParameters}) {
	
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType UserEventScript
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
	
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @governance XXX
	 *
     * @param scriptContext {Object}
     * @param scriptContext.newRecord {Record} New record
     * @param scriptContext.type {UserEventType} Trigger type
     * @param scriptContext.form {Form} Current form
     *
	 * @return {void}
	 *
     * @since 2015.2
	 *
	 * @static
	 * @function beforeLoad
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered after record is saved but before it is committed to database.
     *
     * @governance XXX
	 *
     * @param scriptContext {Object}
     * @param scriptContext.newRecord {Record} New record
     * @param scriptContext.oldRecord {Record} Old record
     * @param scriptContext.type {UserEventType} Trigger type
     *
	 * @return {void}
	 *
     * @since 2015.2
	 *
	 * @static
	 * @function beforeSubmit
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered after record is committed to database.
     *
     * @governance XXX
	 *
     * @param scriptContext {Object}
     * @param scriptContext.newRecord {Record} New record
     * @param scriptContext.oldRecord {Record} Old record
     * @param scriptContext.type {UserEventType} Trigger type
     *
	 * @return {void}
	 *
     * @since 2015.2
	 *
	 * @static
	 * @function afterSubmit
     */
    function afterSubmit(scriptContext) {

    }

	exports.beforeLoad = beforeLoad;
	exports.beforeSubmit = beforeSubmit;
	exports.afterSubmit = afterSubmit;
	return exports;
});
