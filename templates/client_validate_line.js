
	/**
	 * Ensures the line item being added is acceptable
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {Boolean} <code>true</code> to save line item, <code>false</code> to abort save
	 *
	 * @static
	 * @function validateLine
	 */
	function validateLine(type){
	 
		return true;
	}
