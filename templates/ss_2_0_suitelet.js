define([${modules}], function(${moduleParameters}) {
   
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType Suitelet
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @governance XXX
	 *
     * @param context {Object}
     * @param context.request {ServerRequest} incoming request
     * @param context.response {ServerResponse} response
	 *
     * @return {void}
	 *
     * @since 2015.2
	 *
	 * @static
	 * @function onRequest
     */
    function onRequest(context) {

    }

    exports.onRequest = onRequest;
    return exports;
    
});
