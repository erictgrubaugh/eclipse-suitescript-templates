
	/**
	 * Event fired after a database read operation before control is returned to the request client
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 *
	 * @param type {String} Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>view</li>
	 * <li>copy</li>
	 * <li>print</li>
	 * <li>email</li>
	 * </ul>
	 * @param form {nlobjForm} Current form
	 * @param request {nlobjRequest} Request object
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeLoad
	 */
	function beforeLoad(type, form, request){
	 
	}
