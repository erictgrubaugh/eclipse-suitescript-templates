
	/**
	 * Event fired after all dependent fields have been updated
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function postSourcing
	 */
	function postSourcing(type, name) {
	   
	}
