var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Mass Update entry point
	 *
	 * @governance XXX
	 *
	 * @param recType {String} Record type internal id
	 * @param recId {Number} Record internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function run
	 */
	function run(recType, recId) {

	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
