define([${modules}], function(${moduleParameters}) {
	
	/**
     * _moduleDesc
	 *
	 * @exports _moduleId
	 *
     * @NApiVersion 2.x
	 * @NScriptType WorkflowActionScript
	 * @NModuleScope XXX
	 *
	 * @requires _lib
	 *
	 * @copyright ${year} Stoic Software
	 * @author ${author}
	 */
	var exports = {};
	
    /**
     * Entry point for the Workflow Action
     *
	 * @governance XXX
	 *
     * @param scriptContext {Object}
     * @param scriptContext.newRecord {Record} New record
     * @param scriptContext.oldRecord {Record} Old record
	 *
	 * @return {*} Any value can be returned and utilized in workflow
	 *
     * @since 2016.1
	 *
	 * @static
	 * @function onAction
     */
    function onAction(scriptContext) {

    }

	exports.onAction = onAction;
    return exports;
});
