
	/**
	 * Event fired after a database write operation before control is returned to the request client
	 * 
	 * @appliedtorecord recordType
	 *
	 * @governance XXX
	 * 
	 * @param type {String} Operation types:
	 * <ul>
	 * <li>create</li>
	 * <li>edit</li>
	 * <li>delete</li>
	 * <li>xedit</li>
	 * <li>approve</li>
	 * <li>cancel</li>
	 * <li>reject (SO, ER, Time Bill, PO & RMA only)</li>
	 * <li>pack</li>
	 * <li>ship (IF only)</li>
	 * <li>dropship</li>
	 * <li>specialorder</li>
	 * <li>orderitems (PO only)</li>
	 * <li>paybills (vendor payments)</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterSubmit
	 */
	function afterSubmit(type){
	  
	}
