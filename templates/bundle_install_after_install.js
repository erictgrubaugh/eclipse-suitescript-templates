
	/**
	 * Executed after a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 * 
	 * @param toVersion {Number} the version of the bundle that was installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterInstall
	 */
	function afterInstall(toVersion) {
	  
	}
