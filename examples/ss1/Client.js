var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Event fired when the form is initially loaded or reset
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Access mode: create, copy, edit
	 *
	 * @return {void}
	 *
	 * @static
	 * @function pageInit
	 */
	function pageInit(type){
	   
	}

	/**
	 * Ensures the record is acceptable before saving
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @return {Boolean} <code>true</code> to continue save, <code>false</code> to abort save
	 *
	 * @static
	 * @function saveRecord
	 */
	function saveRecord(){

		return true;
	}

	/**
	 * Ensures the new value for a field is acceptable
	 *
	 * @governance XXX
	 * 
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 * @param [index] {Number} line item number, starts from 1
	 * 
	 * @return {Boolean} <code>true</code> to continue changing field value, <code>false</code> to abort value change
	 *
	 * @static
	 * @function validateField
	 */
	function validateField(type, name, index){
	   
		return true;
	}

	/**
	 * Event fired after the value of a field has been modified
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 * @param [index] {Number} line item number, starts from 1
	 *
	 * @return {void}
	 *
	 * @static
	 * @function fieldChanged
	 */
	function fieldChanged(type, name, index){
	 
	}

	/**
	 * Event fired after all dependent fields have been updated
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function postSourcing
	 */
	function postSourcing(type, name) {
	   
	}

	/**
	 * Event fired when a line item is initially selected
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function lineInit
	 */
	function lineInit(type) {
		 
	}

	/**
	 * Ensures the line item being added is acceptable
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {Boolean} <code>true</code> to save line item, <code>false</code> to abort save
	 *
	 * @static
	 * @function validateLine
	 */
	function validateLine(type){
	 
		return true;
	}

	/**
	 * Event triggered when the total amount of the transaction has changed
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {void}
	 *
	 * @static
	 * @function recalc
	 */
	function recalc(type){
	 
	}

	/**
	 * Ensures the line item being inserted is acceptable
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 *
	 * @return {Boolean} <code>true</code> to continue line item insert, <code>false</code> to abort insert
	 *
	 * @static
	 * @function validateInsert
	 */
	function validateInsert(type){
	  
		return true;
	}

	/**
	 * Ensures the line item being deleted can be removed
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 * 
	 * @return {Boolean} <code>true</code> to continue line item delete, <code>false</code> to abort delete
	 *
	 * @static
	 * @function validateDelete
	 */
	function validateDelete(type){
	   
		return true;
	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
