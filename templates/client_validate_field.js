
	/**
	 * Ensures the new value for a field is acceptable
	 *
	 * @governance XXX
	 * 
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 * @param [index] {Number} line item number, starts from 1
	 * 
	 * @return {Boolean} <code>true</code> to continue changing field value, <code>false</code> to abort value change
	 *
	 * @static
	 * @function validateField
	 */
	function validateField(type, name, index){
	   
		return true;
	}
