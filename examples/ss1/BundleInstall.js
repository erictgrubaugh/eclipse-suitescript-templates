var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Executed before a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 *
	 * @param toVersion {Number} the version of the bundle that will be installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeInstall
	 */
	function beforeInstall(toVersion) {
	  
	}

	/**
	 * Executed after a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 * 
	 * @param toVersion {Number} the version of the bundle that was installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterInstall
	 */
	function afterInstall(toVersion) {
	  
	}

	/**
	 * Executed before a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 *
	 * @param fromVersion {Number} the version of the bundle that is currently installed in the target account
	 * @param toVersion {Number} the version of the bundle that will be installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeUpdate
	 */
	function beforeUpdate(fromVersion, toVersion) {
	 
	}

	/**
	 * Executed after a bundle in a target account is updated
	 *
	 * @governance XXX
	 *
	 * @param fromVersion {Number} the version of the bundle that was previously installed in the target account
	 * @param toVersion {Number} the version of the bundle that was just installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterUpdate
	 */
	function afterUpdate(fromVersion, toVersion) {
	  
	}

	/**
	 * Executed before a bundle is uninstalled from a target account
	 *
	 * @governance XXX
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeUninstall
	 */
	function beforeUninstall() {
	 
	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
