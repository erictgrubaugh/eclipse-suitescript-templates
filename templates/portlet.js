
	/**
	 * Portlet entry point
	 *
	 * @governance XXX
	 *
	 * @param portletObj {nlobjPortlet} Current portlet object
	 * @param column {Number} Column position index:
	 * <ul>
	 * <li>1 = left</li>
	 * <li>2 = middle</li>
	 * <li>3 = right</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function render
	 */
	function render(portletObj, column) {

	}
