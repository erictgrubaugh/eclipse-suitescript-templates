
	/**
	 * Executed before a bundle is installed for the first time in a target account
	 *
	 * @governance XXX
	 *
	 * @param toVersion {Number} the version of the bundle that will be installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function beforeInstall
	 */
	function beforeInstall(toVersion) {
	  
	}