define([], function() {

    /**
     * _moduleDesc
     * 
     * @exports _moduleId
     * 
     * @NApiVersion 2.x
     * @NScriptType ScheduledScript
     * @NModuleScope XXX
     * 
     * @requires _lib
     * 
     * @copyright ${year} Stoic Software
     * @author ${author}
     */
    var exports = {};

    /**
     * Definition of the Scheduled script trigger point.
     * 
     * @governance XXX
     * 
     * @param scriptContext
     *        {Object}
     * @param scriptContext.type
     *        {InvocationType} The context in which the script is executed. It
     *        is one of the values from the scriptContext.InvocationType enum.
     * 
     * @return {void}
     * 
     * @since 2015.2
     * 
     * @static
     * @function execute
     */
    function execute(scriptContext) {

    }

    exports.execute = execute;
    return exports;
});
