var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Scheduled Script entry point
	 *
	 * @governance XXX
	 * 
	 * @param type {String} Context Types:
	 * <ul>
	 * <li>scheduled</li>
	 * <li>ondemand</li>
	 * <li>userinterface</li>
	 * <li>aborted</li>
	 * <li>skipped</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function run
	 */
	function run(type) {

	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
