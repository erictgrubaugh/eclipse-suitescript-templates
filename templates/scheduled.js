
	/**
	 * Scheduled Script entry point
	 *
	 * @governance XXX
	 * 
	 * @param type {String} Context Types:
	 * <ul>
	 * <li>scheduled</li>
	 * <li>ondemand</li>
	 * <li>userinterface</li>
	 * <li>aborted</li>
	 * <li>skipped</li>
	 * </ul>
	 *
	 * @return {void}
	 *
	 * @static
	 * @function run
	 */
	function run(type) {

	}
