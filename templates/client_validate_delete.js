
	/**
	 * Ensures the line item being deleted can be removed
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @param type {String} Sublist internal id
	 * 
	 * @return {Boolean} <code>true</code> to continue line item delete, <code>false</code> to abort delete
	 *
	 * @static
	 * @function validateDelete
	 */
	function validateDelete(type){
	   
		return true;
	}
