var _namespace = _namespace || {};

_namespace._moduleName = (function () {
	
	/**
	 * _moduleDesc
	 * 
	 * @exports _moduleId
	 *
	 * @requires _libId
	 *
	 * @copyright 2016 Stoic Software
	 * @author Eric T Grubaugh
	 */
	var exports = {};

	/**
	 * Suitelet entry point
	 *
	 * @governance XXX
	 *
	 * @param request {nlobjRequest} Request object
	 * @param response {nlobjResponse} Response object
	 *
	 * @return {void} Any output is written via response object
	 *
	 * @static
	 * @function run
	 */
	function run(request, response){

	}

	// XXX Ensure proper exports
	
	// Set module.exports for node environment (unit tests)
	if  (typeof module !== 'undefined' && module.exports) {
        module.exports = exports;
    }

	return exports;
})();
