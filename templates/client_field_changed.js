
	/**
	 * Event fired after the value of a field has been modified
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Sublist internal id
	 * @param name {String} Field internal id
	 * @param [index] {Number} line item number, starts from 1
	 *
	 * @return {void}
	 *
	 * @static
	 * @function fieldChanged
	 */
	function fieldChanged(type, name, index){
	 
	}
