
	/**
	 * Ensures the record is acceptable before saving
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 *   
	 * @return {Boolean} <code>true</code> to continue save, <code>false</code> to abort save
	 *
	 * @static
	 * @function saveRecord
	 */
	function saveRecord(){

		return true;
	}
