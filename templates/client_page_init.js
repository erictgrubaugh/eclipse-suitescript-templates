
	/**
	 * Event fired when the form is initially loaded or reset
	 *
	 * @governance XXX
	 *
	 * @appliedtorecord recordType
	 * 
	 * @param type {String} Access mode: create, copy, edit
	 *
	 * @return {void}
	 *
	 * @static
	 * @function pageInit
	 */
	function pageInit(type){
	   
	}
