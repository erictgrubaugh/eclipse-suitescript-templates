
	/**
	 * Executed after a bundle in a target account is updated
	 *
	 * @governance XXX
	 *
	 * @param fromVersion {Number} the version of the bundle that was previously installed in the target account
	 * @param toVersion {Number} the version of the bundle that was just installed in the target account
	 *
	 * @return {void}
	 *
	 * @static
	 * @function afterUpdate
	 */
	function afterUpdate(fromVersion, toVersion) {
	  
	}
